class Clock {

    constructor({template}) {

        this.template = template;
        this.timer;

    }

    render() {

        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template

            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);

    }

    start() {

        this.render();
        let a = this;
        this.timer = setInterval(function(){a.render();}, 1000);

    }

    stop() {

        clearInterval(this.timer);

    }
    
}


var clock = new Clock({template: 'h:m:s'});
clock.start();