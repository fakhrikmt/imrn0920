class Animal {

    constructor(name) {

        this.name           = name;
        this.legs           = 4;
        this.cold_blooded   = false;

    }

}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log('\n')


class Frog extends Animal {

    constructor(name) {

        super(name);
        this.legs = 2;

    }

    jump() {

        console.log("hop hop")

    }

}

class Ape extends Animal {

    constructor(name) {

        super(name);
        this.legs = 2;

    }

    yell() {

        console.log("Auooo")

    }
    
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 