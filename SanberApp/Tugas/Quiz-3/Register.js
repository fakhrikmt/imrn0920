import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'

const Register = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={styles.bagianwel}>
                <Text style={styles.tekswel}>Welcome</Text>
                <Text>Sign up to continue</Text>
            </View>

            <View style={styles.forminput}>
                <View style={styles.kotakform}>
                    <Text>Name</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.kotakform}>
                    <Text>Email</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.kotakform}>
                    <Text>Phone number</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.kotakform}>
                    <Text>Password</Text>
                    <TextInput style={styles.input}/>
                </View>

                <View style={styles.kotakform}>
                    <View style={{marginTop: 20}}>
                        <TouchableOpacity style={styles.btlogin} 
                        onPress={ () => navigation.navigate('DrawerStackScreen') } >
                            <Text style={styles.textbt}>  Sign Up </Text>
                        </TouchableOpacity>
                        <Text style={{color: 'black', marginLeft: 30}}>Already have an account? <Text style={{color: "#F77866"}} onPress={ () => navigation.navigate('Login') }>Sign In</Text></Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 22
    },
    bagianwel: {
        marginTop: 60,

    },
    tekswel: {
        fontSize: 30,
        fontWeight: '700',
        color: "#0C0423",
    },
    kotakform: {
        marginTop: 10,
        marginLeft: 20,
    },
    forminput: {
        marginTop: 70,
        marginLeft: 10,
        marginRight: 30,
        borderWidth: 1,
        borderRadius: 6,
        shadowColor: '#727C8E'

    },
    input: {
        height: 40,
        borderBottomColor: '#003366',
        borderWidth: 1,
        alignItems: 'center',
        width: 255
    },
    kotaklogin: {
        marginTop: 20,
        alignItems: 'center'
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#F77866",
        padding: 10,
        borderRadius: 6,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 255,
        marginLeft: 0
    },
    textbt: {
        color: 'white'
    },
    atautext: {
    
    }
})
