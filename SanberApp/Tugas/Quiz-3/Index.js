import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';


import Login from './Login';
import RegisterScreen from './Register';
import AboutScreen from './Register';
import HomeScreen from './HomeScreen';
import Splash from './Splash';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();


const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if (route.name == 'HomeScreen') {
                return <FontAwesome5 name={'home'} size={size} color={color} solid />
            } else if (route.name == 'Cart') {
                return <FontAwesome5 name="shopping-cart" size={size} color={color} solid />
            } else if (route.name == 'Message') {
                return <FontAwesome5 name="envelope" size={size} color={color} solid />
            } else if (route.name == 'Oke') {
                return <FontAwesome5 name="user" size={size} color={color} solid />
            }
        },
    });
}



const TabsStackScreen = () => (
    <TabsStack.Navigator screenOptions={iconTab} >
        <TabsStack.Screen name='HomeScreen' component={HomeScreen}
            options={{
                title: 'Home'
            }} />
        <TabsStack.Screen name='Cart' component={Splash}
            options={{
                title: 'Cart'
            }}
        />
        <TabsStack.Screen name='Message' component={Splash}
            options={{
                title: 'Message'
            }}
        />
        <TabsStack.Screen name='Oke' component={Splash}
            options={{
                title: 'Profile'
            }}
        />
    </TabsStack.Navigator>
);

const DrawerStackScreen = () => (
    <DrawerStack.Navigator >
        <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}
            options={{
                title: 'Portofolio'
            }} />
        <DrawerStack.Screen name='AboutScreen' component={AboutScreen}
            options={{
                title: 'About'
            }}
        />
    </DrawerStack.Navigator>
);

const RootStackScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name='RegisterScreen' component={RegisterScreen}
            options={{
                title: 'Register'
            }}
        />
        <RootStack.Screen name='Login' component={Login}
            options={{
                title: 'Login'
            }}
        />
        <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen}
            options={{
                title: 'Home'
            }}
        />
    </RootStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);
