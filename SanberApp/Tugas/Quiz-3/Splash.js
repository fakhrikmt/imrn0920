import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

const Splash = () => {
    return (
        <View style={styles.container}>
            <View style={{backgroundColor: '#E5E5E5', marginTop: 150, borderRadius: 200, width: 300, height: 300, flexDirection: 'column'}}>
                <Image source={require('./images/logo.png')} style={{width:230, height:150, marginBottom: 10, marginTop: 80, marginLeft: 35}}/>
            </View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    }
})
