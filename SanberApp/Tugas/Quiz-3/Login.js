import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native'
import { color } from 'react-native-reanimated'

const Register = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={styles.bagianwel}>
                <Text style={styles.tekswel}>Welcome Back</Text>
                <Text>Sign in to continue</Text>
            </View>

            <View style={styles.forminput}>
                <View style={styles.kotakform}>
                    <Text>Email</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.kotakform}>
                    <Text>Password</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.kotakform}>
                    <Text style={{textAlign:'right', marginRight: 20}}>Forgot Password?</Text>
                </View>

                <View style={styles.kotakform}>
                    <View style={{marginTop: 20}}>
                        <TouchableOpacity style={styles.btlogin} 
                        onPress={ () => navigation.navigate('DrawerStackScreen') } >
                            <Text style={styles.textbt}>  Sign In </Text>
                        </TouchableOpacity>
                        <Text style={{color: 'black', marginLeft: 120, marginTop: 20, marginBottom: 20}}>-OR-</Text>
                        <View style={styles.kbmed}>
                            <TouchableOpacity style={styles.btFb} 
                            onPress={ () => navigation.navigate('DrawerStackScreen') } >
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('./images/Fb.png')} style={{width:20, height:20}}/>
                                    <Text style={styles.textbl}>  Facebook </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btGo} 
                            onPress={ () => navigation.navigate('DrawerStackScreen') } >
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('./images/Go.png')} style={{width:20, height:20}}/>
                                    <Text style={styles.textbl}>  Google </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 22
    },
    bagianwel: {
        marginTop: 60,

    },
    tekswel: {
        fontSize: 30,
        fontWeight: '700',
        color: "#0C0423",
    },
    kotakform: {
        marginTop: 10,
        marginLeft: 20,
    },
    forminput: {
        marginTop: 70,
        marginLeft: 10,
        marginRight: 30,
        borderRadius: 6,
        borderColor:'#727C8E', // if you need 
        borderWidth:1,
        overflow: 'hidden',
        shadowColor: 'black',
        shadowRadius: 10,
        shadowOpacity: 1,
    },
    input: {
        height: 40,
        borderBottomColor: '#003366',
        borderWidth: 1,
        alignItems: 'center',
        width: 255
    },
    kotaklogin: {
        marginTop: 20,
        alignItems: 'center'
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#F77866",
        padding: 10,
        borderRadius: 6,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 255,
        marginLeft: 0
    },
    textbt: {
        color: 'white'
    },
    textbl: {
        color: 'black'
    },
    btFb: {
        alignItems: "center",
        backgroundColor: "white",
        overlayColor: "black",
        padding: 10,
        borderRadius: 6,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 110,
        marginLeft: 0
    },
    btGo: {
        alignItems: "center",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 6,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 110,
        marginLeft: 0
    },
    kbmed: {
        flexDirection: 'row',
        marginBottom:2

    },
})
