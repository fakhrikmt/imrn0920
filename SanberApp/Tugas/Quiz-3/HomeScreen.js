import React from 'react'
import { StyleSheet, Text, View, Image, TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const HomeScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.pencarian}>
                <View>
                    <Text style={{color: '#848991', marginLeft: 40}}> Search Product</Text>
                </View>
                <View>
                    <TextInput/>
                </View>
            </View>

            <Image source={require('./images/Slider.png')} style={styles.slide}/>
            
            <View style={{marginBottom: 20}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{borderRadius: 20, marginTop: 20, marginRight: 30, backgroundColor: '#FFBC6C', width: 55, height: 55, alignItems: 'center'}}>
                        <FontAwesome5 name={'tshirt'}
                            size={30} solid
                            color="#EFEFEF"
                            style={{marginTop: 11, marginBottom: 15}}
                        />
                        <Text>Man</Text>
                    </View>

                    <View style={{marginTop: 20, marginRight: 30, backgroundColor: '#44D18C', borderRadius: 20, width: 55, height: 55, alignItems: 'center'}}>
                        <FontAwesome5 name={'tshirt'}
                            size={30} solid
                            color="#EFEFEF"
                            style={{marginTop: 11, marginBottom: 15}}
                        />
                        <Text>Woman</Text>
                    </View>

                    <View style={{marginTop: 20, marginRight: 30, backgroundColor: '#3AD0FF', borderRadius: 20, width: 55, height: 55, alignItems: 'center'}}>
                        <FontAwesome5 name={'tshirt'}
                            size={30} solid
                            color="#EFEFEF"
                            style={{marginTop: 11, marginBottom: 15}}
                        />
                        <Text>Kids</Text>
                    </View>

                    <View style={{marginTop: 20, marginRight: 30, backgroundColor: '#975FF2', borderRadius: 20, width: 55, height: 55, alignItems: 'center'}}>
                        <FontAwesome5 name={'home'}
                            size={30} solid
                            color="#EFEFEF"
                            style={{marginTop: 11, marginBottom: 15}}
                        />
                        <Text>Home</Text>
                    </View>

                    <View style={{marginTop: 20, marginRight: 30, backgroundColor: '#9D9E9F', borderRadius: 20, width: 55, height: 55, alignItems: 'center'}}>
                        <FontAwesome5 name={'chevron-right'}
                            size={30} solid
                            color="#EFEFEF"
                            style={{marginTop: 11, marginBottom: 15}}
                        />
                        <Text>More</Text>
                    </View>
                </View>
            </View>

            <View style={styles.flash}>
                <View style={styles.bgflash}>
                    <Text style={styles.tkflash}>Flash Sell</Text>
                </View>   
    
                <View style={styles.dataflash}>
                    <View style={{marginRight: 5, borderWidth: 1, borderRadius: 5, borderColor: '#f5f5f5'}}>
                        <Image source={require('./images/F1.png')} style={styles.fs}/>
                        <View style={{marginBottom: 10}}>
                            <Text style={styles.textnamaflash}>Tiare Handwash</Text>
                            <Text style={styles.texthargaflash}>$12.22</Text>
                        </View>
                    </View>

                    <View style={{marginRight: 5, borderWidth: 1, borderRadius: 5, borderColor: '#f5f5f5'}}>
                        <Image source={require('./images/F2.png')} style={styles.fs}/>
                        <View style={{marginBottom: 10}}>
                            <Text style={styles.textnamaflash}>Tiare Handwash</Text>
                            <Text style={styles.texthargaflash}>$12.22</Text>
                        </View>
                    </View>

                    <View style={{marginRight: 5, borderWidth: 1, borderRadius: 5, borderColor: '#f5f5f5'}}>
                        <Image source={require('./images/F3.png')} style={styles.fs}/>
                        <View style={{marginBottom: 10}}>
                            <Text style={styles.textnamaflash}>Tiare Handwash</Text>
                            <Text style={styles.texthargaflash}>$12.22</Text>
                        </View>
                    </View>
                </View>
            </View>

            <View style={styles.flash}>
                <View style={{marginBottom: 10}}>
                    <Text style={styles.tknew}>New Product</Text>
                </View>

                <View style={styles.dataproduct}>
                    <View style={styles.produk}>
                        <Image source={require('./images/N1.png')} style={styles.np}/>
                    </View>

                    <View>
                        <Image source={require('./images/N2.png')} style={styles.np}/>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 8,
        marginRight: 8,
        marginTop: 10
    },
    slide: {
        width: 342,
        height: 170,
        borderRadius: 7
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#F77866",
        padding: 10,
        borderRadius: 6,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 255,
        marginLeft: 0
    },
    pencarian: {
        backgroundColor: 'white',
        padding: 10,
        marginBottom: 15,
        borderRadius: 11,
        borderWidth: 1,
        borderColor: '#E5E5E5',
        flexDirection: 'row'
    },
    flash: {
        marginTop: 20
    },
    bgflash: {
        marginBottom: 10
    }, 
    dataflash: {
        flexDirection: 'row'
    },
    tkflash: {
        color: "#4D4D4D", 
        fontSize: 24, 
        fontWeight: "bold"
    },
    fs: {
        width: 150,
        height: 147
    },
    isiflash: {
        borderWidth: 1,
        width: 150,
        height: 60,
        borderRadius: 5,
        borderColor: '#f5f5f5',
        shadowOpacity: 0.9,
        shadowColor: 'black'
    },
    textnamaflash: {
        marginLeft: 10,
        marginTop: 6,
        fontSize: 14
    },
    texthargaflash: {
        marginLeft: 10,
        marginTop: 6,
        fontWeight: 'bold',
        fontSize: 14
    },
    tknew: {
        fontSize: 29,
        color: "#4D4D4D",
        fontWeight: "bold"
    },
    dataproduct: {
        flexDirection: 'row'
    },
    np: {
        width: 163,
        height: 157
    },
    produk: {
        marginRight: 15
    }
})
