import React from 'react'
import {
    Platform, 
    View, 
    Text, 
    Image, 
    ScrollView, 
    TextInput, 
    StyleSheet, 
    Button,
    TouchableOpacity, 
    KeyboardAvoidingView
} from 'react-native'

const RegisterScreen = ({navigation}) => {
    return(
        <KeyboardAvoidingView
            behavior={Platform.OS == "android" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View style={styles.container}>
                    <Image source={require('./images/logo.png')} />
                    <Text style={styles.tekslog}>Register</Text>
                    <View style={styles.bagianinput}>
                        <Text style={styles.teksform}>Username</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.bagianinput}>
                        <Text style={styles.teksform}>Email</Text>
                        <TextInput style={styles.input} textContentType={'emailAddress'} />
                    </View>
                    <View style={styles.bagianinput}>
                        <Text style={styles.teksform} >Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>
                    <View style={styles.bagianinput}>
                        <Text style={styles.teksform}>Ulangi Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>
                    <View style={styles.kotaktombol}>
                        <TouchableOpacity style={styles.tombolmasuk}
                        onPress={ () => navigation.navigate('DrawerStackScreen') } >
                            <Text style={styles.teksbawah}> Daftar ? </Text>
                        </TouchableOpacity>
                        <Text style={styles.ataubawah}>atau</Text>
                        <TouchableOpacity style={styles.tombollogin} 
                        onPress={ () => navigation.navigate('LoginScreen') } >
                            <Text style={styles.teksbawah}>  Masuk </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        flex: 1
    },
    tekslog: {
        fontSize: 24,
        color: '#003366',
        textAlign: "center",
        marginVertical: 20
    },
    teksform: {
        color: '#003366',
    },
    ataubawah: {
        fontSize: 24,
        color: '#3EC6FF',
        textAlign: "center"
    },
    bagianinput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
        borderColor: '#003366',
        borderWidth: 1,
    },
    ttombol: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    tombollogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 10,
        width: 140
    },
    tombolmasuk: {
        alignItems: "center",
        backgroundColor: "#003366",
        textDecorationColor: '#000',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140
    },
    teksbawah: {
        color: 'white',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    kotaktombol: {
        marginTop: 20,
        alignItems: 'center'
    }
})
