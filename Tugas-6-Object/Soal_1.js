function arrayToObject (arr){

    if(arr.length <= 0){

        return console.log(" ")

    }

    for(var i=0; i<arr.length; i++){

        var databaru = { }
        var tahun = arr[i][3];
        var tes = new Date().getFullYear()
        var selisih;

        if (tahun && tes - tahun > 0){

            selisih = tes - tahun

        }else{

            selisih ="invalid Birth Year"

        }

        databaru.firstName  = arr[i][0]
        databaru.lastName   = arr[i][1]
        databaru.gender     = arr[i][2]
        databaru.age        = selisih

        var consoleText = (i + 1) + ' .' + databaru.firstName + ' ' +databaru.lastName + ' : '

        console.log(consoleText)
        console.log(databaru)
    
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

console.log('\n')

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

console.log('\n')

arrayToObject([])
