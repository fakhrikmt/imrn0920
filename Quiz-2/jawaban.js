/*========================================== 
  1. SOAL CLASS SCORE
  ==========================================
*/

class Score {

    constructor(subject, email, points) {
    
        this.email    = email;
        this.subject  = subject;
        this.points   = points;
      

    }
  
    average() {

        let jumlah = 0;

        for (let i = 0; i < this.points.length; i++) {

            jumlah += this.points[i]

        }

        return (jumlah / this.points.length).toFixed(1)

    }

  }

console.log('==========================================')
console.log('2. SOAL Create Score')
console.log('==========================================')

console.log('\n')
  
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
  
function viewScores(data, subject) {
 
    let sesi;
    let temp = []

    switch (subject) {

        case "quiz-1": sesi = 1; break;
        case "quiz-2": sesi = 2; break;
        case "quiz-3": sesi = 3; break;
        default: break;

    }

    for (let i = 1; i < data.length; i++) {

        const akhir = new Score(subject, data[i][0], data[i][sesi])
        temp.push(akhir)
      
    }

    console.log(temp)

  }

  
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


console.log('\n')
  

console.log('==========================================')
console.log('3. SOAL Recap Score')
console.log('==========================================')

console.log('\n')
  
function recapScores(data) {

    let temp = []

    for (let i = 1; i < data.length; i++) {

        const [email, ...jum] = data[i]
        const score = new Score('Recap', data[i][0], jum)
        const total = score.average()
  
        let hitung;

        if (total > 90) {

            hitung = 'honour'

        } else if (total > 80) {

            hitung = 'graduate'

        } else if (total > 70) {

            hitung = 'participant'

        }
  
        const tampil =
            `     ${i}. Email: ${score.email}
        Rata-rata: ${total}
        rangking: ${hitung}
          `
  
        console.log(tampil)
    }
}

  
recapScores(data);
  