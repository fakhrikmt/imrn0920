function sum (mulai,akhir,step){
    var array = [];
    var jarak;

    if(!step){

        jarak = 1

    }else{

        jarak = step

    }

    if(mulai > akhir){

        var data = mulai;
        for ( var i = 0; data >= akhir; i++){
            array.push(data)
            data -= jarak
        }

    }else if(mulai < akhir){

        var data = mulai;
        for ( var i = 0; data <= akhir; i++){

            array.push(data)
            data += jarak

        }

    }else if(!mulai && !akhir && !step){

        return 0

    }else if (mulai){

        return mulai

    }

    var total = 0;
    for (var i = 0; i<array.length; i++){

        total = total+array[i]
    }

    return total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 