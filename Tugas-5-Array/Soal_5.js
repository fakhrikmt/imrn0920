function balikKata(balik){

    let data = "";

    for (var i = balik.length -1; i>=0; i--){
        data = data + balik[i];
    }
    return data;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 